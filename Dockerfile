FROM opensuse/leap

RUN zypper ref --build-only && \
    zypper -n in bind && \
    zypper clean --all
